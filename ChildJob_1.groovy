import groovy.json.JsonSlurper;

JENKINS_PIPELINE_FOLDER = 'kafka-etc-wip'  
def REPO_BASE_URL = "https://git.nbnco.net.au/analytics"
def PIPELINE_REPO_NAME = "titans-shared-analytics-infrastructure-build-tools"
def PIPELINE_REPO_FOLDER = ""

def releaseScript = readFileFromWorkspace('sharp_jobs/jobTemplates/scriptedPipelines/configuration.json')
def InputJSON = new JsonSlurper().parseText(releaseScript)

Map pipeline_config = [:]
InputJSON.each{ pipeline_name, pipeline_props ->
  pipeline_config[pipeline_name] = [
    job_name: InputJSON['titans-ShARP-cleanser-dev']['job_name'],
    display_name: InputJSON['titans-ShARP-cleanser-dev']['display_name'],
    description: InputJSON['titans-ShARP-cleanser-dev']['description'],
    pipeline_repo_name: InputJSON['titans-ShARP-cleanser-dev']['pipeline_repo_name'],
    dsl_job_file: InputJSON['titans-ShARP-cleanser-dev']['dsl_job_file'],
  ]
}

pipeline_config.each { pipeline_name, pipeline_props ->
  def job_builder = pipelineJob("${pipeline_props['job_name']}") {
    logRotator {
      numToKeep(20)
    }
    description(pipeline_props['description'])
    keepDependencies(false)
    displayName(pipeline_props['display_name'])
    disabled(false)
    }
  definition_builder(job_builder, pipeline_props, REPO_BASE_URL, PIPELINE_REPO_FOLDER)
  }



  def definition_builder(def job_builder, pipeline_props, repo_base_url, pipeline_repo_folder) {
  job_builder.with {
    definition{
      cpsScm {
        scriptPath(pipeline_repo_folder + pipeline_props['dsl_job_file'])
        scm {
          git {
            branch("refs/heads/feature/SEOENG-26405")
            remote {
              name(pipeline_props['pipeline_repo_name'])
              url(repo_base_url + '/' + pipeline_props['pipeline_repo_name'] + ".git")
              credentials('SERVICE_ACCOUNT')
            }
            extensions {
              cleanAfterCheckout()
              disableRemotePoll()
              localBranch()

            }
          }
        }
      }
    }
  }
}


